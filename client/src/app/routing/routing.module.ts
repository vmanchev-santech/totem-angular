import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ArtistSearchComponent} from '../artist-search/artist-search.component';
import {ArtistAlbumsComponent} from '../artist-albums/artist-albums.component';
import {AlbumComponent} from '../album/album.component';

const routes: Routes = [
  {path: '', redirectTo: '/search', pathMatch: 'full'},
  {path: 'search', component: ArtistSearchComponent},
  {path: 'artist/:id', component: ArtistAlbumsComponent},
  {path: 'album/:id', component: AlbumComponent}
]

@NgModule({
  imports: [

    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class RoutingModule {}
