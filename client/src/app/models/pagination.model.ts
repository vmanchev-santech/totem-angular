export class PaginationModel {

  public totalPages: number;
  public currentPage: number;
  public prevPage: number;
  public prevOffset: number;
  public nextPage: number;
  public nextOffset: number
  public firstVisiblePage = 1;
  public lastVisiblePage: number;
  public perPage = 20;
  public maxPagesToDisplay = 5;
  public pagesRange: number[];

  constructor(
    public total: number,
    public offset: number,
    public limit: number,
    public stateName: string,
    public keyword?: string,
    public entityId?: string
  ) {

    if (total === 0) {
      return;
    }

    this.totalPages = Math.ceil(total / 20);
    this.currentPage = (this.offset === 0) ? 1 : (this.offset / this.perPage) + 1;

    this.setup();
  }

  setup() {

    //number of search results is not more than the number of results per page
    if (this.total <= this.perPage) {
      return;
    }

    this.createVisiblePagesRange();

    if (this.currentPage + 1 <= this.totalPages) {
      this.nextPage = this.currentPage + 1;
      this.nextOffset = this.currentPage * this.perPage;
    } else {
      this.nextPage = null;
      this.nextOffset = this.perPage;
    }

    if (this.currentPage - 1 > 0) {
      this.prevPage = this.currentPage - 1;
      this.prevOffset = (this.prevPage - 1) * this.perPage;
    } else {
      this.prevOffset = 0;
    }

  }

  createVisiblePagesRange() {

    this.pagesRange = [];

    this.firstVisiblePage = this.currentPage - 2;

    if (this.firstVisiblePage < 1) {
      this.firstVisiblePage = 1;
    }

    this.lastVisiblePage = this.currentPage + 2;

    if (this.lastVisiblePage < this.maxPagesToDisplay) {
      this.lastVisiblePage = this.maxPagesToDisplay;
    }

    if (this.lastVisiblePage > this.totalPages) {
      this.lastVisiblePage = this.totalPages;
    }

    for (let i = this.firstVisiblePage; i <= this.lastVisiblePage; i++) {
      this.pagesRange.push(i);
    }
  }
}
