export class EntityModel {
  public id: string;
  public name: string;
  public type: string;
  public uri: string;
  public href: string;
  public external_urls: any;
  public images: any;
}