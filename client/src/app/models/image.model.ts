export class ImageModel {

  constructor(
    public url: string,
    public width?: number,
    public height?: number) {

  }
}