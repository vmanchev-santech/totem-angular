import {EntityModel} from './entity.model';

export class ArtistModel extends EntityModel {
  public followers: any;
  public genres: any;
  public popularity: number
}
