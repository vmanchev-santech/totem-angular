import {EntityModel} from './entity.model';

export class AlbumModel extends EntityModel {
  public available_markets: any[];
  public artists: any[];
  public album_type: number;
  public tracks?: any;
}