import {ArtistModel} from './artist.model';

export class ArtistsResultsModel {
  public href: string;
  public limit: number;
  public next: string;
  public offset: number;
  public previous: string;
  public total: number;
  public items: ArtistModel[];
};