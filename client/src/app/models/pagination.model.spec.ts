import {PaginationModel} from '../models/pagination.model';
import {async, fakeAsync, flush, ComponentFixture, TestBed} from '@angular/core/testing';

describe('PaginationModel', () => {

  describe('constructor', () => {

    it('should do nothing, if total results is zero', () => {

      let paginationModel = new PaginationModel(0, 0, 20, 'search', 'elvis');

      spyOn(paginationModel, 'setup');

      expect(paginationModel.totalPages).toBeUndefined();
      expect(paginationModel.currentPage).toBeUndefined();
      expect(paginationModel.setup).not.toHaveBeenCalled();
    });

    it('should start the calculations, if total results are greater than zero', () => {

      let paginationModel = new PaginationModel(100, 0, 20, 'search', 'elvis');

      expect(paginationModel.totalPages).toBe(5);
      expect(paginationModel.currentPage).toBe(1);
    });

  });

  describe('setup', () => {

    it('shold do nothing if total results are less then or equal the results per page', () => {
      let paginationModel = new PaginationModel(20, 0, 20, 'search', 'elvis');
      expect(paginationModel.prevPage).toBeUndefined();
      expect(paginationModel.nextPage).toBeUndefined();
    });

    it('should should calculate previous page, if current page is not the first page', () => {
      let paginationModel = new PaginationModel(100, 20, 20, 'search', 'elvis');
      expect(paginationModel.prevPage).toBeDefined();
    });
  });

  describe('createVisiblePagesRange', () => {

    it('should set lastVisiblePage to totalPages, if caluclated value of lastVisiblePage is greater than the number of pages',
      () => {
        let paginationModel = new PaginationModel(100, 60, 20, 'search', 'elvis');
        expect(paginationModel.lastVisiblePage).toEqual(5);
      })

  });
});