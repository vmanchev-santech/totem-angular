import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from '../services/spotify.service';
import { ResultsModel } from '../models/results.model';
import { PaginationModel } from '../models/pagination.model';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-artist-search',
  templateUrl: './artist-search.component.html',
  styleUrls: ['./artist-search.component.css']
})
export class ArtistSearchComponent implements OnInit {

  form: FormGroup;
  public results: ResultsModel;
  public pagination: PaginationModel;
  @BlockUI() blockUI: NgBlockUI;

  public keyword: string;

  constructor(
    public fb: FormBuilder,
    public spotifyService: SpotifyService,
    private route: ActivatedRoute
  ) {

  }

  ngOnInit() {
    this.route.params.subscribe(
      (params: any) => {
        this.getPage(params.keyword, params.offset);
        this.createForm();
      });
  }

  createForm() {
    this.form = this.fb.group({
      keyword: [this.keyword, Validators.required]
    });
  }

  searchArtists() {

    if (!this.form.valid) {
      return;
    }

    this.getPage(this.form.value.keyword);

  }

  getPage(keyword: string, offset?: number) {

    if (typeof keyword === 'undefined') {
      return;
    }

    keyword = keyword.trim();

    if (keyword.length === 0) {
      return;
    }

    this.keyword = keyword;

    if (typeof offset === 'undefined') {
      offset = 0;
    }

    this.blockUI.start();

    window.scrollTo(0, 0);

    this.spotifyService
      .search(keyword, offset, environment.pagination.resultsPerPage)
      .subscribe(

      (results: any) => {
        this.blockUI.stop();
        this.results = results.artists as ResultsModel;
        this.pagination = new PaginationModel(
          this.results.total,
          this.results.offset,
          this.results.limit,
          '/search',
          keyword);
      }

      );

  }
}
