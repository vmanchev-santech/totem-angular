import {NO_ERRORS_SCHEMA} from '@angular/core';
import {fakeAsync, async, tick, flush, ComponentFixture, TestBed} from '@angular/core/testing';
import {AlbumComponent} from './album.component';
import {AlbumModel} from '../models/album.model';
import {TrackDurationPipe} from '../pipes/track-duration.pipe';
import {SpotifyService} from '../services/spotify.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';

class SpotifyServiceMock {

  public album: AlbumModel;

  getAlbum() {
    this.album = new AlbumModel();
    this.album.id = 'asdf';
    return Observable.of(this.album);
  }
}

describe('AlbumComponent', () => {
  let component: AlbumComponent;
  let fixture: ComponentFixture<AlbumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AlbumComponent,
        TrackDurationPipe
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      providers: [
        {provide: SpotifyService, useClass: SpotifyServiceMock}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('getAlbum', () => {

    beforeEach(() => {
      spyOn(component.spotifyService, 'getAlbum').and.callThrough();
    });

    it('should get album by id', fakeAsync(() => {

      component.getAlbum('test4');

      expect(component.spotifyService.getAlbum).toHaveBeenCalledWith('test4');

      flush();

      fixture.detectChanges();

      expect(fixture.componentInstance.album instanceof AlbumModel).toBe(true);
      expect(fixture.componentInstance.album.id).toBe('asdf');

    }));
  });


});
