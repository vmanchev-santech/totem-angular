import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SpotifyService} from '../services/spotify.service';
import {AlbumModel} from '../models/album.model';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit {

  public album: AlbumModel;

  constructor(
    public spotifyService: SpotifyService,
    private route: ActivatedRoute
  ) {

  }

  ngOnInit() {
    this.route.params.subscribe(
      (params: any) => this.getAlbum.call(this, params.id));
  }

  getAlbum(id: string) {

    if (typeof id === 'undefined') {
      return;
    }

    this.spotifyService.getAlbum(id).subscribe(
      (album: AlbumModel) => this.album = album
    );
  }
}
