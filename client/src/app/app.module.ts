import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BlockUIModule} from 'ng-block-ui';
import {RoutingModule} from './routing/routing.module';
import {AppComponent} from './app.component';
import {ArtistSearchComponent} from './artist-search/artist-search.component';
import {SpotifyService} from './services/spotify.service';
import {ArtistGenresPipe} from './pipes/artist-genres.pipe';
import {DisplayImageComponent} from './display-image/display-image.component';
import {ArtistAlbumsComponent} from './artist-albums/artist-albums.component';
import {AlbumComponent} from './album/album.component';
import {TrackDurationPipe} from './pipes/track-duration.pipe';
import {PaginationComponent} from './pagination/pagination.component';

@NgModule({
  declarations: [
    AppComponent,
    ArtistSearchComponent,
    ArtistGenresPipe,
    DisplayImageComponent,
    ArtistAlbumsComponent,
    AlbumComponent,
    TrackDurationPipe,
    PaginationComponent
  ],
  imports: [
    BrowserModule,
    RoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BlockUIModule
  ],
  providers: [SpotifyService],
  bootstrap: [AppComponent]
})
export class AppModule {}
