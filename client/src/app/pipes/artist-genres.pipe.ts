import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'artistGenres'
})
export class ArtistGenresPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (!value || !value.genres || typeof value.genres.join !== 'function') {
      return null;
    }

    return value.genres.join(', ');
  }

}
