import { ArtistGenresPipe } from './artist-genres.pipe';

let pipe: any;

describe('ArtistGenresPipe', () => {

  beforeEach(() => {
    pipe = new ArtistGenresPipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should transform array of genres into a comma separated string', () => {
    expect(pipe.transform()).toEqual(null);
    expect(pipe.transform({ id: 1 })).toEqual(null);
    expect(pipe.transform({ genres: 1 })).toEqual(null);
    expect(pipe.transform({ genres: ['aaa', 'bbb', 'ccc'] })).toEqual('aaa, bbb, ccc');
  });
});
