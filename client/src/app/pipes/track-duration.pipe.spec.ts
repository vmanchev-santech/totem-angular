import { TrackDurationPipe } from './track-duration.pipe';

let pipe: any;

describe('TrackDurationPipe', () => {

  beforeEach(() => {
    pipe = new TrackDurationPipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should display milliseconds in minutes and seconds', () => {
    expect(pipe.transform(149386)).toEqual('2:29');
    expect(pipe.transform(60500)).toEqual('1:01');
  });

});
