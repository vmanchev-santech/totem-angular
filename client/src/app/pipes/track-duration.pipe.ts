import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'trackDuration'
})
export class TrackDurationPipe implements PipeTransform {

  transform(value: any, args?: any): any {

    const minutes = Math.floor(value / 60000);
    const seconds = parseInt(((value % 60000) / 1000).toFixed(0), 10);

    return minutes + ':' + (seconds < 10 ? '0' : '') + seconds;
  }

}
