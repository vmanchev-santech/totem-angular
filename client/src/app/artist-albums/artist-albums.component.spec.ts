import {NO_ERRORS_SCHEMA} from '@angular/core';
import {fakeAsync, async, flush, ComponentFixture, TestBed} from '@angular/core/testing';
import {ArtistAlbumsComponent} from './artist-albums.component';
import {SpotifyService} from '../services/spotify.service';
import {ResultsModel} from '../models/results.model';
import {PaginationModel} from '../models/pagination.model';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';

class SpotifyServiceMock {

  public resultsMock: ResultsModel;

  getArtistAlbums() {

    this.resultsMock = new ResultsModel();

    return Observable.of(this.resultsMock);
  }
}

describe('ArtistAlbumsComponent', () => {
  let component: ArtistAlbumsComponent;
  let fixture: ComponentFixture<ArtistAlbumsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ArtistAlbumsComponent],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      providers: [
        {provide: SpotifyService, useClass: SpotifyServiceMock}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistAlbumsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('getArtistAlbums', () => {

    beforeEach(() => {
      spyOn(component.spotifyService, 'getArtistAlbums').and.callThrough();
      spyOn(component.blockUI, 'start');
      spyOn(component.blockUI, 'stop');
      spyOn(window, 'scrollTo');
    })

    it('should set offset to 0 if offset is not provided', () => {
      component.getArtistAlbums('asdf');
      expect(component.spotifyService.getArtistAlbums).toHaveBeenCalledWith('asdf', 0, 20)
    });

    it('should use the provided offset value', () => {
      component.getArtistAlbums('asdf', 40);
      expect(component.spotifyService.getArtistAlbums).toHaveBeenCalledWith('asdf', 40, 20);
    });

    it('should blovk the user interface and scroll the window to top', () => {
      component.getArtistAlbums('asdf');
      expect(window.scrollTo).toHaveBeenCalledWith(0, 0);
      expect(component.blockUI.start).toHaveBeenCalled();
    });

    it('should unblock the UI in case of success', fakeAsync(() => {

      component.getArtistAlbums('asdf');

      expect(component.spotifyService.getArtistAlbums).toHaveBeenCalledWith('asdf', 0, 20);

      flush();

      fixture.detectChanges();

      expect(typeof fixture.componentInstance.albums).toBeDefined();
      expect(component.pagination instanceof PaginationModel).toBe(true);
    }));
  });
});
