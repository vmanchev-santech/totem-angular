import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SpotifyService} from '../services/spotify.service';
import {ResultsModel} from '../models/results.model';
import {AlbumModel} from '../models/album.model';
import {PaginationModel} from '../models/pagination.model';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-artist-albums',
  templateUrl: './artist-albums.component.html',
  styleUrls: ['./artist-albums.component.css']
})
export class ArtistAlbumsComponent implements OnInit {

  public artistId: string;
  public albums: AlbumModel[];
  public pagination: PaginationModel;
  @BlockUI() blockUI: NgBlockUI;

  constructor(
    public spotifyService: SpotifyService,
    private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.params.subscribe(
      (params: any) => this.getArtistAlbums.call(this, params.id, params.offset));
  }

  getArtistAlbums(artistId: string, offset?: number) {

    offset = (typeof offset === 'undefined') ? 0 : offset;

    this.blockUI.start();

    window.scrollTo(0, 0);

    this.spotifyService.getArtistAlbums(artistId, offset, environment.pagination.resultsPerPage).subscribe(
      (albumsResponse: ResultsModel) => {

        this.blockUI.stop();

        this.albums = albumsResponse.items;

        this.pagination = new PaginationModel(
          albumsResponse.total,
          albumsResponse.offset,
          albumsResponse.limit,
          '/artist',
          null,
          artistId
        );

      })

  }

}
