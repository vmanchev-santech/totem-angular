import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { SpotifyService } from './spotify.service';
import { environment } from '../../environments/environment';

const mockArtistSearchResponse = {
  artists: {
    href: 'https://api.spotify.com/v1/search?query=elvis&type=artist&offset=0&limit=20',
    limit: 20,
    next: 'https://api.spotify.com/v1/search?query=elvis&type=artist&offset=20&limit=20',
    offset: 0,
    previous: null,
    total: 420,
    items: [{
      external_urls: {
        spotify: 'https://open.spotify.com/artist/43ZHCT0cAZBISjO8DG9PnE'
      },
      followers: {
        href: null,
        total: 1833090
      },
      genres: [
        'adult standards',
        'christmas',
        'rock-and-roll',
        'rockabilly'
      ],
      href: 'https://api.spotify.com/v1/artists/43ZHCT0cAZBISjO8DG9PnE',
      id: '43ZHCT0cAZBISjO8DG9PnE',
      images: [
        {
          height: 640,
          url: 'https://i.scdn.co/image/160c941240375cecbdebfe61033e59783c5172ab',
          width: 640
        },
        {
          height: 320,
          url: 'https://i.scdn.co/image/4e6bf82acdc3e175e06e902f5c1b32eaab2fe0c0',
          width: 320
        },
        {
          height: 160,
          url: 'https://i.scdn.co/image/e3e5fd6d8a70dc5f94438fbe880fb1a8844026c4',
          width: 160
        }
      ],
      name: 'Elvis Presley',
      popularity: 81,
      type: 'artist',
      uri: 'spotify:artist:43ZHCT0cAZBISjO8DG9PnE'
    }]
  }
};

const mockArtistByIdResponse = {
  href: 'https://api.spotify.com/v1/artists/43ZHCT0cAZBISjO8DG9PnE',
  id: '43ZHCT0cAZBISjO8DG9PnE',
  name: 'Elvis Presley',
  type: 'artist',
  uri: 'spotify:artist:43ZHCT0cAZBISjO8DG9PnE'
};

const mockAlbumsSearchResponse = {
  href: '',
  items: [{}, {}, {}],
  limit: 20,
  next: '',
  offset: 0,
  previous: null,
  total: 631
};

const mockAlbumResponse = {
  id: '11FCLUM5m9GiuxjGEoTVF5',
  images: [{}],
  tracks: {
    items: [{}]
  }
};

describe('SpotifyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SpotifyService]
    });
  });

  it('should be created', inject([SpotifyService], (service: SpotifyService) => {
    expect(service).toBeTruthy();
  }));

  describe('search', () => {
    it('should perform a valid request to search by type and keyword',
      inject([HttpTestingController, SpotifyService], (httpMock: HttpTestingController, service: SpotifyService) => {

        service.search('test4', 0, 20).subscribe(response => {
          expect(response).toEqual(mockArtistSearchResponse);
        });

        const rq = httpMock.expectOne('http://localhost:3000/api/search/test4?limit=20&offset=0');

        rq.flush(mockArtistSearchResponse);

        expect(rq.request.method).toEqual('GET');

        httpMock.verify();

      }));
  });

  describe('getArtist', () => {
    it('should get an artits by id',
      inject([HttpTestingController, SpotifyService], (httpMock: HttpTestingController, service: SpotifyService) => {

        service.getArtist('43ZHCT0cAZBISjO8DG9PnE').subscribe(response => {
          expect(response).toEqual(mockArtistByIdResponse);
        });

        const rq = httpMock.expectOne('http://localhost:3000/api/artist/43ZHCT0cAZBISjO8DG9PnE');

        rq.flush(mockArtistByIdResponse);

        expect(rq.request.method).toEqual('GET');

        httpMock.verify();

      }));
  });

  describe('searchAlbumbs', () => {
    it('should perform request to search albumns by artist id',
      inject([HttpTestingController, SpotifyService], (httpMock: HttpTestingController, service: SpotifyService) => {

        service.getArtistAlbums('43ZHCT0cAZBISjO8DG9PnE', 0, 20).subscribe(
          success => {
            expect(success).toEqual(mockAlbumsSearchResponse);
          }
        );

        const rq = httpMock.expectOne({
          url: environment.api.base + '/artist/43ZHCT0cAZBISjO8DG9PnE/albums?limit=20&offset=0',
          method: 'GET'
        });

        rq.flush(mockAlbumsSearchResponse);

        expect(rq.request.method).toEqual('GET');

        httpMock.verify();
      }));
  });

  describe('getAlbum', () => {
    it('should perform request to get album by id',
      inject([HttpTestingController, SpotifyService], (httpMock: HttpTestingController, service: SpotifyService) => {

        service.getAlbum('11FCLUM5m9GiuxjGEoTVF5').subscribe(
          success => {
            expect(success).toEqual(mockAlbumResponse);
          }
        );

        const rq = httpMock.expectOne({
          url: environment.api.base + '/album/11FCLUM5m9GiuxjGEoTVF5',
          method: 'GET'
        });

        rq.flush(mockAlbumResponse);

        expect(rq.request.method).toEqual('GET');

        httpMock.verify();
      }));
  });
});
