import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable()
export class SpotifyService {

  constructor(
    private httpClient: HttpClient) {}

  search(keyword: string, offset: number, limit: number) {
    return this.httpClient.get(`${environment.api.base}/search/${keyword}?limit=${limit}&offset=${offset}`);
  }

  getArtist(id: string) {
    return this.httpClient.get(`${environment.api.base}/artist/${id}`);
  }

  getArtistAlbums(id: string, offset: number, limit: number) {
    return this.httpClient.get(`${environment.api.base}/artist/${id}/albums?limit=${limit}&offset=${offset}`);
  }

  getAlbum(id: string) {
    return this.httpClient.get(`${environment.api.base}/album/${id}`);
  }

}
