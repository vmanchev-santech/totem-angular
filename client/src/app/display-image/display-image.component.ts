import { Component, OnInit, Input } from '@angular/core';
import { ImageModel } from '../models/image.model';

@Component({
  selector: 'app-display-image',
  templateUrl: './display-image.component.html'
})
export class DisplayImageComponent implements OnInit {

  @Input() imageList: ImageModel[];
  @Input() imageType = 'thumbnail';
  @Input() altText = '';

  public selectedImage: ImageModel;

  ngOnInit() {

    // build a getter method name, using the passed image type
    const method = 'get' + this.capitalizeFirstLetter(this.imageType);

    // call the getter and set selectedImage
    if (typeof this[method] !== 'undefined') {
      this[method]();
    }
  }

  getThumbnail() {

    if (this.imageList.length === 0) {

      this.selectedImage = new ImageModel('http://placehold.it/100x100', 100, 100);

      return this.selectedImage;
    }

    // get the smallest image in the list
    this.selectedImage = this.imageList.reduce(function (prev, curr) {
      return prev.width < curr.width ? prev : curr;
    });

    this.selectedImage.width = 100;
    this.selectedImage.height = 100;

    return this.selectedImage;
  }

  getCard() {

    if (this.imageList.length === 0) {

      this.selectedImage = new ImageModel('http://placehold.it/300x300', 300, 300);

      return this.selectedImage;
    }

    // get the smallest image in the list
    this.selectedImage = this.imageList.reduce(function (prev, curr) {
      return prev.width < curr.width ? curr : prev;
    });

    this.selectedImage.width = 300;
    this.selectedImage.height = 300;

    return this.selectedImage;
  }

  getPoster() {

    if (this.imageList.length === 0) {

      this.selectedImage = new ImageModel('http://placehold.it/640x640', 640, 640);

      return this.selectedImage;
    }

    // get the smallest image in the list
    this.selectedImage = this.imageList.reduce(function (prev, curr) {
      return prev.width < curr.width ? curr : prev;
    });

    this.selectedImage.width = 640;
    this.selectedImage.height = 640;

    return this.selectedImage;
  }

  capitalizeFirstLetter(word: string) {
    return word.charAt(0).toUpperCase() + word.slice(1);
  }
}
