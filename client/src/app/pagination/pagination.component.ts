import { Component, Input } from '@angular/core';
import { PaginationModel } from '../models/pagination.model';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent {

  @Input() data: PaginationModel;

}
