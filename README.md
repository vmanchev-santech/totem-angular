# Totem Spotify

## Install

To install the required NPM modules for both client and server side, run the 
following command from project's root folder:

```
git clone git@bitbucket.org:vmanchev-santech/totem-angular.git
cd totem-angular
npm run totem:install
```
## Run the project

To build the client and start the backend server, run the following command:

```
npm run totem:start
```

Open your browser on `http://localhost:3000` and perform an artist search.

## Configuration

### Backend server configuration

There is a custom Spotify module available for the backend server. It relays on 
**client_credentials** authentication option. The base clientId and clientSecret
can be found at `server/config.js` file. Feel free to replace these with your own.

### Frontend client

Configuration files for frontend client can be found at `client/src/environments`. 
Currently there are two configuration options:

- **api.base** - base url of the backend server where all the requests will be 
sent to.
- **pagination.resultsPerPage** - how many results should be displayed per page. 
This value is used for both artist search results page and albums search results 
page.
