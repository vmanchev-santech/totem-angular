const express = require('express');
const path = require('path');
const http = require('http');
const fs = require('fs');
const config = require('./config');
const url = require("url");
const cors = require('cors')
const spotify = require('./spotify');

/* eslint-disable no-console */

const port = process.env.PORT || 3000;

const app = express();

app.use(express.static(path.join(__dirname, '../public')));
app.use(cors());

app.get('/api/search/:keyword', function (req, res) {

  let params = Object.assign(
    {},
    url.parse(req.url, true).query,
    req.params
    );

  spotify.action(
    'searchArtistsByKeyword',
    params,
    (error, response, body) => {
    res.json(body);
  });

});

app.get('/api/artist/:id', function (req, res) {

  spotify.action(
    'getArtistById',
    req.params.id,
    (error, response, body) => {
    res.json(body);
  });

});

app.get('/api/artist/:id/albums', function (req, res) {

  let params = Object.assign(
    {},
    url.parse(req.url, true).query,
    req.params
    );

  spotify.action(
    'getArtistAlbums',
    params,
    (error, response, body) => {
    res.json(body);
  });

});

app.get('/api/album/:id', function (req, res) {
  spotify.action(
    'getAlbumById',
    req.params.id,
    (error, response, body) => {
    res.json(body);
  });
});

app.get('*', function (req, res) {
  res.sendFile(path.join(__dirname, '../public/index.html'));
});

app.listen(port, function (err) {
  if (err) {
    console.log(err);
  } else {
    console.log('Server listening : http://localhost:%s', port);
  }
});

